new Vue({
    el: '#app',
    data: {
        currentNavTab: 'settings',
        settings: {
            players: ['Nicki', 'Gast'],
            points: 301,
            throws: 3,
            firstShotHasToBeDouble: false,
            anyShotHasToBeDouble: false,
            startingPlayerId: undefined,
            nightmode: nightmodeIsActive()
        },
        score: {
            round: 1,
            roundPlayed: [],
            currentPlayerId: undefined,
            players: []
        },
        inProgress: false
    },
    methods: {
        navigate: function(target) {
            this.currentNavTab = target;
        },
        showModal: function(id) {
            document.getElementById(id).showModal();
        },
        closeModel: function(id) {
            document.getElementById(id).close();
        },
        initGame: function() {
            this.resetScore();
            this.showModal('playerpick');
        },
        startGame: function() {
            this.closeModel('playerpick');
            this.score.currentPlayerId = this.settings.startingPlayerId;
            this.currentNavTab = 'game';
            this.inProgress = true;
        },
        resetScore: function() {
            this.score = {
                round: 1,
                roundPlayed: [],
                currentPlayerId: undefined,
                players: Array.from({length: this.settings.players.length}).fill(this.settings.points)
            };
        },
        restartGame: function() {
            this.showModal('restartquestion');
        },
        restartGameYes: function() {
            this.closeModel('restartquestion');
            this.resetScore();
            this.inProgress = false;
            this.currentNavTab = 'settings';
        },
        addScore: function(playerId, score) {
            const prevScore = this.score.players[playerId];
            const currentScore = prevScore - score;

            if (currentScore > 0) {
                this.score.players[playerId] -= score;
                return;
            }
            
            if (currentScore === 0) {
                this.score.winnerId = playerId;
                this.showModal('winner');
                this.inProgress = false;
                this.currentNavTab = 'settings';
                return;
            }

            if (currentScore < 0) {
                this.showModal('toomuch');
            }
        },
        next: function() {
            this.score.roundPlayed.push(this.score.currentPlayerId);
            this.addScore(this.score.currentPlayerId, getRoundScore());

            if (this.score.roundPlayed.length === this.settings.players.length) {
                ++this.score.round;
                this.score.roundPlayed = [];
                this.score.currentPlayerId = this.settings.startingPlayerId;
                this.resetGameTap();
                return;
            }

            if (this.score.currentPlayerId === this.settings.players.length - 1) {
                this.score.currentPlayerId = 0;
            } else {
                ++this.score.currentPlayerId;
            }
            
            this.resetGameTap();
        },
        resetGameTap: function() {
            Array.prototype.forEach.call(document.querySelectorAll('#game [type=range], #game [type=radio]'), x => {
                x.disabled = false;
                x.checked = x.type === 'radio' && x.value === '1';

                if (x.type === 'range') {
                    x.value = 0;
                    printRangeValue(x);
                    calculateThrowScore(x);
                }
            });
            calculateRoundScore();
            window.scroll(0, 0);
        },
        toggleNightmode: function(e) {
            document.body.classList.toggle('nightmode', this.settings.nightmode);
            localStorage.setItem('dartboard-settings-nightmode', this.settings.nightmode);
        }
    },
    computed: {

    }
});

(function() {
    document.body.classList.toggle('nightmode', nightmodeIsActive());
})();

function nightmodeIsActive() {
    return localStorage.getItem('dartboard-settings-nightmode') === true.toString();
}

Array.prototype.forEach.call(document.querySelectorAll('[type=range]'), x => {
    x.addEventListener('input', () => {
        rerangeValue(x);
        printRangeValue(x);
        calculateThrowScore(x);
        enableMultiplicator(x);
        calculateRoundScore();
    });
});

Array.prototype.forEach.call(document.querySelectorAll('#game [type=radio]'), x => {
    x.addEventListener('change', () => {
        calculateThrowScore(x);
        calculateRoundScore();
    });
});

function rerangeValue(input) {
    if (input.value > 20 && input.value < 38) {
        input.value = 25;
    }
    if (input.value > 25) {
        input.value = 50;
    }
}

function printRangeValue(input) {
    if (!input || !input.id) return;
    const target = document.getElementById(`${input.id}-value`);
    if (!target) return;

    target.textContent = input.value;
}

function calculateThrowScore(input) {
    const thro = input.dataset.throw;
    
    document.getElementById(`throw${thro}-score`).textContent = getThrowScore(thro);
}

function getThrowScore(thro) {
    let result = parseInt(document.getElementById(`throw${thro}-points`).value);

    if (result > 20) return result;

    return result * parseInt(document.querySelector(`[name="throw${thro}-multiplier"]:checked`).value);
}

function enableMultiplicator(input) {
    const thro = input.dataset.throw;
    Array.prototype.forEach.call(document.querySelectorAll(`[name="throw${thro}-multiplier"]`), x => {
        x.disabled = input.value > 20;

        if (x.disabled && x.value == '1') {
            x.checked = true;
        }
    });
}

function calculateRoundScore() {
    document.getElementById('roundscore-value').textContent = getRoundScore();
}

function getRoundScore() {
    return Array.prototype.reduce.call(document.querySelectorAll('[data-throwscore]'), (sum, x) => {
        return sum += parseInt(x.textContent);
    }, 0);
}